import csv
from bs4 import BeautifulSoup as soup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import re
import os

chromedriver = "/media/inno/My_Projects/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver)

# driver = webdriver.Firefox()
driver.maximize_window()

driver.get('https://www.linkedin.com/')

user_name = 'ersiyaram@gmail.com' 
user_password = 'Siyaram421'

username = driver.find_element_by_id('login-email')
username.send_keys(user_name)

password = driver.find_element_by_id('login-password')
password.send_keys(user_password)

driver.find_element_by_name('submit').click()
time.sleep(5)

currnt_info 	 = csv.writer(open('Current_InformationMe.csv','ab'))
past_info 		 = csv.writer(open('Past_InformationMe.csv','ab'))
educational_info = csv.writer(open('Educational_InformationMe.csv','ab'))

c_header = ['LinkedIn ID','Name','Class','Current Organization','Current Designation', 'Current Industry','Current_Location','Background']
e_header = ['LinkedIn_Id', 'Name', 'School/College', 'Degree', 'Major', 'Year']
p_header = ['LinkedIn ID','Name','Class','Past Organization','Past Designation', 'Past Location','From_Month_Year', 'To_Month_Year']

currnt_info.writerow(c_header)
educational_info.writerow(e_header)
past_info.writerow(p_header)

reader = csv.reader(open('LinkedIn_Ids.csv','rb'))

unicod = u'\u2013'

for row in reader:
	driver.get(row[0])
	time.sleep(7)
	import pdb;pdb.set_trace()
	source =  soup(driver.page_source.encode('utf-8'))

# 	# Current Data

	try:
		name = driver.find_element_by_class_name('full-name').text.encode('utf-8').replace('\n','').replace('\t','').replace('\r','').strip()
	except Exception,e:
		name = ''

	f_class = '2002'

	currnt_data = driver.find_element_by_class_name('current-position').text.split('\n')
	try:
		crrent_organization = currnt_data[1]
	except Exception,e:
		crrent_organization = ''

	try:
		crrent_designation = currnt_data[0]
	except Exception,e:
		crrent_designation = ''

	try:
		current_industry = driver.find_element_by_name('industry').text.encode('utf-8').replace('\n','').replace('\t','').replace('\r','').strip()
	except Exception,e:
		current_industry = ''

	try:
		currnt_location = driver.find_element_by_name('location').text.encode('utf-8').replace('\n','').replace('\t','').replace('\r','').strip()
	except Exception,e:
		currnt_location = ''	

	try:
		background = driver.find_element_by_id('background').text.encode('utf-8').replace('\n','').replace('|','').replace('\t','').replace('\r','').strip('BackgroundSummary')
	except Exception,e:
		background = ''

	currnt_info.writerow([row[0],name,f_class, crrent_organization,crrent_designation,current_industry,currnt_location,background])
	
 
	education_data = driver.find_elements_by_class_name('education')

	for education in education_data:
		edu_info = education.text.split('\n')

		if len(edu_info) == 2:
			school   = edu_info[0].strip()
			degree 	 = ''
			major    = ''
			edu_date = edu_info[1].replace(unicod, '-').encode('utf-8').strip()


		else:
			# import pdb;pdb.set_trace()
			school   = edu_info[0].strip()
			try:
				degree_1   = edu_info[1].split(',')
				degree   = degree_1[0].strip()
				major  	 = degree_1[1].strip()
				edu_date = edu_info[2].replace(unicod, '-').encode('utf-8').strip()
			except:
				degree = ''
				major = ''
				edu_date = ''

		educational_info.writerow([row[0], name, school, degree, major, edu_date])

	
	past_datas = driver.find_elements_by_class_name('past-position')

	for past_data in past_datas:
		past_data = past_data.text.split('\n')

		try:
			past_organization = past_data[1]
		except Exception,e:
			past_organization = ''

		try:
			past_designation = past_data[0]
		except Exception,e:
			past_designation = ''

		try:
			past_location = past_data[2].split(')')[-1].strip()
		except Exception,e:
			past_location = ''		

		from_month_year = past_data[2].split(unicod)[0].strip()
		to_month_year 	= past_data[2].split(unicod)[-1].split('(')[0].strip()

		past_info.writerow([row[0],name,f_class, past_organization, past_designation, past_location, from_month_year, to_month_year])
