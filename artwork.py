import requests
import csv
import re
import os
import time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains

def data_extraction():
	data_soup = BeautifulSoup(driver.page_source.encode('utf-8'))

	for data_type in data_soup.find_all('a',{'data-type':'artwork'}):
		title_ 			= data_type.text.strip().encode('utf-8')
		artist_         = data_type.find_previous('td', {'class':'column-2'}).text.strip().encode('utf-8')	
		ini_pur_price   = data_type.find_next('td', {'class':'column-4'}).text.strip().encode('utf-8')
		hol_peri_years  = data_type.find_next('td', {'class':'column-5'}).text.strip().encode('utf-8')
		last_rpt_sale   = data_type.find_next('td', {'class':'column-6'}).text.strip().encode('utf-8')
		annualized_err  = data_type.find_next('td', {'class':'column-7'}).text.strip().encode('utf-8')
		
		try:
			driver.find_element_by_link_text(title_).click()
			time.sleep(5)

			artwork_soup = BeautifulSoup(driver.page_source.encode('utf-8'))
			artwork_data = artwork_soup.find('span',{'class':'artwork-info'})

			artist  = artwork_data.find('h2').text.strip().encode('utf-8')
			title   = artwork_data.find('h1').text.strip().encode('utf-8')
			year    = artwork_data.find('h3').text.strip().encode('utf-8') 
			art_d   = artwork_data.find_all('p')

			medium 			= art_d[0].text.strip().encode('utf-8')
			size 			= art_d[1].text.strip().encode('utf-8')
			auction_house 	= art_d[2].text.strip().encode('utf-8')
			location 		= art_d[3].text.strip().encode('utf-8')
			auction_date 	= art_d[4].text.strip().encode('utf-8')
			lot 			= art_d[5].text.strip().encode('utf-8')
			sale 			= art_d[6].text.strip().encode('utf-8')

			table_data 		= artwork_soup.find('table', {'class':'table-model-artwork'}).find_all('td', {'class':re.compile(r'column-2')})
			
			low_estimate 	= table_data[0].text.strip().encode('utf-8')
			high_estimate 	= table_data[1].text.strip().encode('utf-8')
			hammer_price 	= table_data[2].text.strip().encode('utf-8')
			premium_price 	= table_data[3].text.strip().encode('utf-8')
			err 			= table_data[4].text.strip().encode('utf-8')

			try:
				image = artwork_soup.find('img', {'class':'artwork-small'}).get('src')
			except:
				image = 'N/A'


			writable_data = [artist, title, ini_pur_price, hol_peri_years, last_rpt_sale, annualized_err, year, medium, size, 
			auction_house, location, auction_date, lot, sale, low_estimate, high_estimate, hammer_price, premium_price, err, image]
			
			
			print writable_data
			mycsv.writerow(writable_data)

			driver.find_element_by_class_name('close-overlay').click()
			time.sleep(2)

		except:
			unsuccess = [title_, artist_, annualized_err]
			new_csv.writerow(unsuccess)
			print unsuccess
			pass

	driver.execute_script("window.scrollTo(document.body.scrollHeight, 0);")
	driver.find_element_by_link_text('NEXT').click()
	time.sleep(5)

	data_extraction()


mycsv = csv.writer(open('ArtWorkSale.csv', 'ab'))
# header = ['Artist', 'Title', 'Initial Purchase Price', 'Holding period, years', 'Last repeat sale price', 'Annualized ERR', 'Year', 
#'Medium', 'Size', 'Auction House', 'Location', 'Auction Date', 'Lot', 'Sale/Collection', 'Low Estimate(USD)', 'High Estimate(USD)', 
#'Hammer Price(USD)', 'Premium Price(USD)', 'ERR(%)', 'Image']
# mycsv.writerow(header)

new_csv = csv.writer(open('Unsuccessful_Title.csv', 'ab'))
# head = ['Unsuccessful_Titles']
# new_csv.writerow(head)

chromedriver = "/media/inno/My_Projects/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver)

driver.maximize_window()
driver.get('http://www.skatepress.com/login/')

time.sleep(3)
username = driver.find_element_by_id("login_form_user_login")
password = driver.find_element_by_id("login_form_user_pass")

username.send_keys("Siyar")
password.send_keys("SRM@75000")

driver.find_element_by_id("do_login").click()
time.sleep(8)

driver.get('http://www.skatepress.com/skates-top-10000/repeat-sales/14/')
data_extraction()

